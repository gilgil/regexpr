#include <iostream>
#include <QCoreApplication>
#include <QFile>
#include <QRegularExpression>
#include <QTextStream>

using namespace std;

void usage() {
	cout << "syntax : regexpr <expression> <file name>" << endl;
	cout << "sample : regexpr '\\[bob[0-9]\\]\\[([^\\]]*)\\]([^\\[]*)\\[([^\\]]*)\\]</span>, <span>([^<]*)' input.txt" << endl;
}

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);
	if (argc != 3) {
		usage();
		return 0;
	}
	QRegularExpression re(argv[1]);
	QFile file(argv[2]);
	if (!file.open(QIODevice::ReadOnly)) {
		fprintf(stderr, "can not open file %s\n", argv[2]);
		return -1;
	}
	QTextStream ts(&file);
	QString text = ts.readAll();

	int offset = 0;
	while (true) {
		QRegularExpressionMatch match = re.match(text, offset);
		if (!match.hasMatch()) break;
		int captureIndex = 0;
		while (true) {
			QString captured = match.captured(captureIndex);
			if (captured.isNull()) break;
			if (captureIndex > 0)
				cout << "\t";
			cout << qPrintable(captured);
			captureIndex++;
		}
		offset = match.capturedEnd();
		cout << endl;
	}
}
